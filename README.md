# README #

This README describes how to add more papers into the literatures repository.

### Steps to follow ###

1. Upload the paper (any file format, e.g pdf, word) with the git CLI.
2. Edit the Wiki page to add the references to the papers.
    * The format of the reference should be: first author et. al, PAPER-NAME, conference/journal **brief name**.
    * E.g, Matthew Halpern et. al, Mobile CPU’s Rise to Power: Quantifying the Impact of Generational Mobile CPU Design Trends on Performance, Energy, and User Satisfaction, **HPCA16**.

### [Start surfing](https://bitbucket.org/jsicsar/literatures/wiki/Home) ###